export interface IPhoto {
  albumId: number;
  id: number;
  title: string;
  url: string;
  thumbnailUrl: string;
}

export interface IPropsPhoto {
  photo: IPhoto;
}

export interface ILoadCurrentPhotosArguments {
  page: number;
  value: string;
}
