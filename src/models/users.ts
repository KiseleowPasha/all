import { createEffect, createStore } from 'effector';
import { IUser } from '../types/users';

export const loadUsers = createEffect(async (): Promise<any> => {
  try {
    const data: Response = await fetch(
      'https://jsonplaceholder.typicode.com/users'
    );
    return data.json();
  } catch (e) {
    console.log(e);
  }
});

export const $users = createStore<IUser[]>([]).on(
  loadUsers.doneData,
  (_, users: IUser[]): IUser[] => users
);

export const loadCurrentUser = createEffect(
  async (userId: string | undefined): Promise<any> => {
    try {
      const data = await fetch('https://jsonplaceholder.typicode.com/users');
      const users = await data.json();
      const id = Number(userId);
      const currentUser = users.filter((user: IUser) => user.id === id);
      return { ...currentUser[0] };
    } catch (e) {
      console.log(e);
    }
  }
);

export const $currentUser = createStore<IUser>({}).on(
  loadCurrentUser.doneData,
  (_, user: IUser): IUser => user
);
