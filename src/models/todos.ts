import { createEffect, createEvent, createStore } from 'effector';
import { ITodo } from '../types/todos';

export const loadTodos = createEffect(async (): Promise<any> => {
  try {
    const data: Response = await fetch(
      'https://jsonplaceholder.typicode.com/todos'
    );
    return data.json();
  } catch (e) {
    console.log(e);
  }
});

export const loadCompletedTodos = createEffect(async (): Promise<any> => {
  try {
    const data: Response = await fetch(
      'https://jsonplaceholder.typicode.com/todos'
    );
    const todos: ITodo[] = await data.json();
    return todos.filter((todo: ITodo) => todo.completed);
  } catch (e) {
    console.log(e);
  }
});

export const loadUnCompletedTodos = createEffect(async (): Promise<any> => {
  try {
    const data: Response = await fetch(
      'https://jsonplaceholder.typicode.com/todos'
    );
    const todos: ITodo[] = await data.json();
    return todos.filter((todo: ITodo) => !todo.completed);
  } catch (e) {
    console.log(e);
  }
});

export const completeTodo = createEvent<ITodo>();

export const $todos = createStore<ITodo[]>([])
  .on(loadTodos.doneData, (_, todos: ITodo[]): ITodo[] => todos)
  .on(completeTodo, (state: ITodo[], todo: ITodo): ITodo[] => {
    todo.completed = !todo.completed;
    return [...state];
  });

export const $completedTodos = createStore<ITodo[]>([])
  .on(loadCompletedTodos.doneData, (_, todos: ITodo[]): ITodo[] => todos)
  .on(completeTodo, (state: ITodo[], todo: ITodo): ITodo[] => {
    return todo.completed
      ? [...state, todo]
      : state.filter((todoInState: ITodo) => todoInState.id !== todo.id);
  });

export const $unCompletedTodos = createStore<ITodo[]>([])
  .on(loadUnCompletedTodos.doneData, (_, todos: ITodo[]): ITodo[] => todos)
  .on(completeTodo, (state: ITodo[], todo: ITodo): ITodo[] => {
    return !todo.completed
      ? [...state, todo]
      : state.filter((todoInState: ITodo) => todoInState.id !== todo.id);
  });
