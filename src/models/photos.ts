import { createEffect, createEvent, createStore } from 'effector';
import { ILoadCurrentPhotosArguments, IPhoto } from '../types/photos';

export const loadPhotos = createEffect(async (): Promise<any> => {
  try {
    const data: Response = await fetch(
      'https://jsonplaceholder.typicode.com/photos'
    );
    return data.json();
  } catch (e) {
    console.log(e);
  }
});

export const loadCurrentPhotos = createEffect(
  async (info: ILoadCurrentPhotosArguments): Promise<any> => {
    try {
      const data: Response = await fetch(
        'https://jsonplaceholder.typicode.com/photos'
      );
      const photos: IPhoto[] = await data.json();
      const count: number = info.page - 1;
      return info.value
        ? photos.filter((photo: IPhoto) => photo.title.includes(info.value))
        : photos.filter(
            (photo: IPhoto) =>
              photo.id > count * 100 && photo.id <= info.page * 100
          );
    } catch (e) {
      console.log(e);
    }
  }
);

export const $photos = createStore<IPhoto[]>([]).on(
  loadPhotos.doneData,
  (_, photos: IPhoto[]): IPhoto[] => photos
);

export const $currentPhotos = createStore<IPhoto[]>([]).on(
  loadCurrentPhotos.doneData,
  (_, photos: IPhoto[]) => photos
);

export const changeValueInInput = createEvent<string>();

export const $valueInInput = createStore<string>('').on(
  changeValueInInput,
  (state: string, value: string): string => value
);
