import { useStore } from 'effector-react';
import { FC, useEffect, useState } from 'react';
import {
  $completedTodos,
  $todos,
  $unCompletedTodos,
  loadTodos,
  loadUnCompletedTodos,
  loadCompletedTodos,
} from '../../models/todos';
import { ITodo } from '../../types/todos';
import { Todo } from './todo/todo';

export const Todos: FC = () => {
  const todos: ITodo[] = useStore($todos);
  const completedTodos: ITodo[] = useStore($completedTodos);
  const unCompletedTodos: ITodo[] = useStore($unCompletedTodos);
  const [current, setCurrent] = useState<string>('all');
  console.log(completedTodos);
  let currentTodos: ITodo[] = [];
  if (current === 'all') currentTodos = todos;
  if (current === 'completed') currentTodos = completedTodos;
  if (current === 'uncompleted') currentTodos = unCompletedTodos;
  useEffect((): void => {
    loadTodos();
    loadCompletedTodos();
    loadUnCompletedTodos();
  }, []);
  return (
    <div className='todos'>
      <button onClick={(): void => setCurrent('all')}>show all</button>
      <button onClick={(): void => setCurrent('completed')}>
        show completed
      </button>
      <button onClick={(): void => setCurrent('uncompleted')}>
        show uncompleted
      </button>
      <h1>TODOS:</h1>
      <ul>
        {currentTodos.map((todo: ITodo) => (
          <li key={todo.id}>
            <Todo todo={todo} />
          </li>
        ))}
      </ul>
    </div>
  );
};
