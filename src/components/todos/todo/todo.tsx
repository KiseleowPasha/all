import { FC } from 'react';
import { completeTodo } from '../../../models/todos';
import { ITodoProps } from '../../../types/todos';

export const Todo: FC<ITodoProps> = ({ todo }) => {
  return (
    <div className='todo'>
      <label>
        <input
          type='checkbox'
          checked={todo.completed}
          onChange={() => completeTodo(todo)}
        />
        {todo.title}
      </label>
    </div>
  );
};
