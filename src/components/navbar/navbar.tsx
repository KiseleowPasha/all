import { FC } from 'react';
import { NavLink } from 'react-router-dom';

export const Navbar: FC = () => {
  return (
    <nav>
      <ul>
        <li>
          <NavLink to={'/users'}>Users</NavLink>
        </li>
        <li>
          <NavLink to={'/photos/1'}>Photos</NavLink>
        </li>
        <li>
          <NavLink to={'/todos'}>Todos</NavLink>
        </li>
      </ul>
    </nav>
  );
};
