import { useStore } from 'effector-react';
import { FC, useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import { $users, loadCurrentUser, loadUsers } from '../../models/users';
import { IUser } from '../../types/users';

export const Users: FC = () => {
  const users: IUser[] = useStore($users);
  useEffect((): void => {
    loadUsers();
    loadCurrentUser(undefined);
  }, []);
  return (
    <div className='users'>
      <h1>Users: </h1>
      <ul>
        {users.map((user: IUser) => (
          <li key={user.id}>
            <NavLink to={`/users/${user.id}`}>{user.name}</NavLink>
          </li>
        ))}
      </ul>
    </div>
  );
};
