import { useStore } from 'effector-react';
import { FC, useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import { $currentUser, loadCurrentUser } from '../../../models/users';
import { IUsersParams } from '../../../types/users';

export const User: FC = () => {
  const { id }: IUsersParams = useParams();
  const currentUser = useStore($currentUser);
  useEffect((): void => {
    loadCurrentUser(id);
  }, []);
  return (
    <div className='user'>
      <NavLink to={'/users'}>Back</NavLink>
      <h4>Page of user: {currentUser.name}</h4>
      <h5>UserName: {currentUser.username}</h5>
      <h5>Email: {currentUser.email}</h5>
      <h5>City: {currentUser.address?.city}</h5>
    </div>
  );
};
