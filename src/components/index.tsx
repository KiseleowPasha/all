import { FC } from 'react';
import { HashRouter as Router, Route, Routes } from 'react-router-dom';
import { Greeting } from './greeting';
import { Navbar } from './navbar/navbar';
import { Photos } from './photos';
import { Todos } from './todos';
import { Users } from './users';
import { User } from './users/user/user';

export const App: FC = () => {
  return (
    <Router>
        <Navbar />
      <Routes>
      <Route path={'/'} element={<Greeting />} />
        <Route path={'/photos/:page'} element={<Photos />} />
        <Route path={'/todos'} element={<Todos />} />
        <Route path={'/users'} element={<Users />} />
        <Route path={'/users/:id'} element={<User />}/>
      </Routes>
    </Router>
  );
};
