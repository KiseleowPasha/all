import { useStore } from 'effector-react';
import { FC, useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import {
  $currentPhotos,
  $photos,
  $valueInInput,
  loadCurrentPhotos,
  loadPhotos,
} from '../../models/photos';
import { IPhoto } from '../../types/photos';
import { Input } from './input/input';
import { Photo } from './photo/photo';

export const Photos: FC = () => {
  const photos: IPhoto[] = useStore<IPhoto[]>($photos);
  const page: number = Number(useParams<{ page: string }>().page);
  const currentPhotos = useStore<IPhoto[]>($currentPhotos);
  const valueInInput = useStore<string>($valueInInput);
  useEffect((): void => {
    loadPhotos();
  }, []);
  useEffect(() => {
    loadCurrentPhotos({ page: page, value: valueInInput });
  }, [page, valueInInput]);
  return (
    <div className='photos'>
      <h1>PHOTOS:</h1>
      <Input />
      {currentPhotos.map((photo: IPhoto) => (
        <Photo key={photo.id} photo={photo} />
      ))}
      {valueInInput ? null : (
        <>
          <NavLink to={page > 1 ? `/photos/${page - 1}` : `/photos/${page}`}>
            prev
          </NavLink>
          <NavLink
            to={
              page < photos.length / 100
                ? `/photos/${page + 1}`
                : `/photos/${page}`
            }
          >
            next
          </NavLink>
        </>
      )}
    </div>
  );
};
