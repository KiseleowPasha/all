import { FC } from 'react';
import { IPropsPhoto } from '../../../types/photos';

export const Photo: FC<IPropsPhoto> = ({ photo }) => {
  return (
    <div className='photo'>
      <h4>{photo.title}</h4>
      <img src={photo.url} alt='Photo' width={50} />
    </div>
  );
};
