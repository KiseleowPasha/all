import { useStore } from 'effector-react';
import { ChangeEvent, FC } from 'react';
import { $valueInInput, changeValueInInput } from '../../../models/photos';

export const Input: FC = () => {
  const value = useStore($valueInInput);
  return (
    <input
      type='text'
      value={value}
      onChange={(event: ChangeEvent<HTMLInputElement>) =>
        changeValueInInput(event.target.value)
      }
    />
  );
};
